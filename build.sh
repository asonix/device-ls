#!/usr/bin/env bash

set -xe

flatpak-builder build dog.asonix.git.asonix.DeviceLs.yml --user --install --force-clean
